---
version: 2
type de projet: Projet de semestre 5
année scolaire: 2020/2021
titre: Plateforme d’analyse d’image pour détection des tissus sains et non sains
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 2
mandants: 
  - Département de médecine/pathologie UNIFR - Dr. Jimmy Stalin et Prof. Curzio Ruegg
professeurs co-superviseurs:
  - Cyril Berton
  - Karl Daher
mots-clés: [image anylsis, Color Analysis, Anomaly Detection]
langue: [F,E]
confidentialité: non
suite: non
---

```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{IMG/cell.png}
\end{center}
```

## Description/Contexte

Dans le cadre d’une étude en pathologie médicale, nous souhaitons développer une plateforme sur laquelle nous pourrions traiter et analyser des images des tissus pulmoniares. 
Le projet se concentre sur la détection des cellules ou tissus sain et pas-sains. Nous nous concentrons sur la détection de l’altération pulmonaire dans la fibrose et la détection de différents types de cellules dans les poumons. 
Le tissue est normalement coloré et scanné par un microscope. 
Type de coloration : 
- Coloration rouge pour les tissus pas sains 
- Coloration bleu violet pour les tissus pas sains
- Coloration brune est utilisée pour la détection de cellules spécifiques  
Les images sont transférées par le scanner sous l’extension ‘.ndpi’, les détails du software sont sous ce lien : 
https://nanozoomer.hamamatsu.com/jp/en/product/search/U12388-01/index.html 

Nous avons besoin de définir 1/ les sections d’intérêt (alvéole pulmonaire) puis de définir 2/ s'il s’agit de tissus sains ou pas-sains et 3/ la présence et la quantification de cellules d’intérêt pour le suivi de l’évolution de la maladie. 
Les résultats seront analysés en parallèle par un expert de recherche medical afin de valider le processus.

## Objectifs/Tâches

- Analyse du dataset fourni et prise en main des logiciels du projet 
- Recherche des librairies d’analyse d’image (openCV…)
- Recherche des technologies pour développer la platforme
- Création du prototype
- Test et validation 

